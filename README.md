# seenthis/autoembed

AutoEmbed - PROGRAMMABLE EMBEDDED MEDIA CLASS

## Usage

`/spip.php?action=api_autoembed&url=chatquivole`

## Apache Rewrite Rules

Le fichier `htaccess` fourni par SPIP est suffisant pour permettre :

`/autoembed.api/?url=chatquivole`

### Retrocompatibilité

Dans le fichier `.htaccess` de SPIP, au niveau des `REGLAGES PERSONNALISES` :

- `RewriteRule ^autoembed/$ autoembed.api [QSA,L]`
